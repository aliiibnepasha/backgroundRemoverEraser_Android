package com.ma.backgroundremovereraser.helpers;


import com.ma.backgroundremovereraser.R;

import java.util.ArrayList;
import java.util.List;

public class ListHelper {

    public static List<String> getColorsList(){

        List<String> listOfColors = new ArrayList<String>();
        listOfColors.add("#FFBB86FC");
        listOfColors.add("#FF6200EE");
        listOfColors.add("#FF3700B3");
        listOfColors.add("#FF03DAC5");
        listOfColors.add("#FF018786");
        listOfColors.add("#FF000000");
        listOfColors.add("#1C1A1F");
        listOfColors.add("#28C84B");
        listOfColors.add("#4285F4");
        listOfColors.add("#FF9800");
        listOfColors.add("#FFBB86FC");
        listOfColors.add("#FF6200EE");
        listOfColors.add("#FF3700B3");
        listOfColors.add("#FF03DAC5");
        listOfColors.add("#FF018786");
        listOfColors.add("#FF000000");
        listOfColors.add("#1C1A1F");
        listOfColors.add("#28C84B");
        listOfColors.add("#4285F4");
        listOfColors.add("#FF9800");
        listOfColors.add("#FFBB86FC");
        listOfColors.add("#FF6200EE");
        listOfColors.add("#FF3700B3");
        listOfColors.add("#FF03DAC5");
        listOfColors.add("#FF018786");
        listOfColors.add("#FF000000");
        listOfColors.add("#1C1A1F");
        listOfColors.add("#28C84B");
        listOfColors.add("#4285F4");
        listOfColors.add("#FF9800");


        return listOfColors;
    }
    public static List<Integer> getGradientsList(){

        List<Integer> listOfGradients = new ArrayList<Integer>();

        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);
        listOfGradients.add(R.drawable.ic_gradient3);
        listOfGradients.add(R.drawable.ic_gradient4);
        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);
        listOfGradients.add(R.drawable.ic_gradient3);
        listOfGradients.add(R.drawable.ic_gradient4);
        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);
        listOfGradients.add(R.drawable.ic_gradient3);
        listOfGradients.add(R.drawable.ic_gradient4);
        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);
        listOfGradients.add(R.drawable.ic_gradient3);
        listOfGradients.add(R.drawable.ic_gradient4);
        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);
        listOfGradients.add(R.drawable.ic_gradient3);
        listOfGradients.add(R.drawable.ic_gradient4);
        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);
        listOfGradients.add(R.drawable.ic_gradient3);
        listOfGradients.add(R.drawable.ic_gradient4);
        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);
        listOfGradients.add(R.drawable.ic_gradient3);
        listOfGradients.add(R.drawable.ic_gradient4);
        listOfGradients.add(R.drawable.ic_gradient1);
        listOfGradients.add(R.drawable.ic_gradient2);

        return listOfGradients;
    }
    public static List<Integer> getPatternsList(){

        List<Integer> listOfPatterns = new ArrayList<Integer>();

        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);
        listOfPatterns.add(R.drawable.ic_pattern3);
        listOfPatterns.add(R.drawable.ic_pattern4);
        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);
        listOfPatterns.add(R.drawable.ic_pattern3);
        listOfPatterns.add(R.drawable.ic_pattern4);
        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);
        listOfPatterns.add(R.drawable.ic_pattern3);
        listOfPatterns.add(R.drawable.ic_pattern4);
        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);
        listOfPatterns.add(R.drawable.ic_pattern3);
        listOfPatterns.add(R.drawable.ic_pattern4);
        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);
        listOfPatterns.add(R.drawable.ic_pattern3);
        listOfPatterns.add(R.drawable.ic_pattern4);
        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);
        listOfPatterns.add(R.drawable.ic_pattern3);
        listOfPatterns.add(R.drawable.ic_pattern4);
        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);
        listOfPatterns.add(R.drawable.ic_pattern3);
        listOfPatterns.add(R.drawable.ic_pattern4);
        listOfPatterns.add(R.drawable.ic_pattern1);
        listOfPatterns.add(R.drawable.ic_pattern2);

        return listOfPatterns;
    }

}

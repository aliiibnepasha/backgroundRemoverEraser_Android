package com.ma.backgroundremovereraser.bgremover

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.ma.backgroundremovereraser.*
import com.ma.backgroundremovereraser.activities.BackgroundChangeActivity
import com.ma.backgroundremovereraser.activities.MainActivity
import com.ma.backgroundremovereraser.databinding.ActivityManualRemoveBinding
import com.ma.backgroundremovereraser.helpers.OnBackgroundChangeListener
import com.github.dhaval2404.imagepicker.ImagePicker
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class ManualRemoveActivity : AppCompatActivity() , View.OnClickListener {


//    private val imagePath: String? = null
//    private var mBitmapRemove: String? = null
//    private val mContentResolver: ContentResolver? = null
//    var mImageView: ImageView? = null
    private var mBitmap: Bitmap? = null
    private var BitMapForMaualEdit: Bitmap? = null

    var mHoverView: HoverView? = null
    var mDensity = 0.0

    var viewWidth = 0
    var viewHeight = 0
    var bmWidth = 0
    var bmHeight = 0

    var actionBarHeight = 0
    var bottombarHeight = 0
    var bmRatio = 0.0
    var viewRatio = 0.0

    var eraserMainButton: Button? = null
    var magicWandMainButton:android.widget.Button? = null
    var mirrorButton:android.widget.Button? = null
    var positionButton:android.widget.Button? = null
    var eraserSubButton: ImageView? = null
    var unEraserSubButton:android.widget.ImageView? = null
    var brushSize1Button: ImageView? = null
    var brushSize2Button:android.widget.ImageView? = null
    var brushSize3Button:android.widget.ImageView? = null
    var brushSize4Button:android.widget.ImageView? = null
    var magicRemoveButton: ImageView? = null
    var magicRestoreButton:android.widget.ImageView? = null
    var undoButton: ImageView? = null
    var redoButton:android.widget.ImageView? = null
    var nextButton: Button? = null
    var colorButton: ImageView? = null
    var magicSeekbar: SeekBar? = null
    var eraserLayout: RelativeLayout? = null
    var magicLayout:RelativeLayout? = null
    var mLayout: RelativeLayout? = null

    var currentColor = 0
    private val RESULT_LOAD_IMG = 1
    private val REQUEST_IMAGE_CROP = 2
    var imgDecodableString: String? = null
    private lateinit var binding: ActivityManualRemoveBinding

    private lateinit var uriImagePicked : Uri


    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
        binding = ActivityManualRemoveBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        imageResult.launch("image/*")

//        ImagePicker.with(this)
//            .crop()	    			//Crop image(Optional), Check Customization for more option
//            .start()

        ImagePicker.with(this)
            .crop()
            .galleryOnly()
            .createIntent { intent ->
                startForProfileImageResult.launch(intent)
            }

        binding.removeBgBtn.setOnClickListener {


            var intent = Intent(this,BackgroundChangeActivity::class.java)
            intent.putExtra("uriImagePicked",uriImagePicked.toString())
            startActivity(intent)
            finish()

//            removeBg()

//            binding.removeBackGround.visibility = View.INVISIBLE
//            binding.manualRemove.visibility = View.VISIBLE
        }
        binding.nextButton.setOnClickListener {
            saveImage()
        }
    }


    private fun saveImage() {
        //save drawing
        val saveDialog = AlertDialog.Builder(this)
        saveDialog.setTitle("Save Image")
        saveDialog.setMessage("Save Image to device Gallery?")
        saveDialog.setPositiveButton("Yes", object : DialogInterface.OnClickListener {
            private val fOut: FileOutputStream? = null
            override fun onClick(dialog: DialogInterface, which: Int) {
                //save drawing
//                val well: Bitmap = myDrawView.getBitmap()

//        for any layout use to creat bitmap
                binding.mainLayout.invalidate()
                  binding.mainLayout.setDrawingCacheEnabled(true)
                  val well: Bitmap = binding.mainLayout.getDrawingCache()
                  val root = Environment.getExternalStorageDirectory().toString()

                val save = Bitmap.createBitmap(320, 480, Bitmap.Config.ARGB_8888)
                val paint = Paint()
                paint.color = Color.WHITE
                val now = Canvas(save)
                now.drawRect(Rect(0, 0, 320, 480), paint)
                now.drawBitmap(well, Rect(0, 0, well.width, well.height), Rect(0, 0, 320, 480), null)
                //attempt to save
                val imgSaved = MediaStore.Images.Media.insertImage(contentResolver, save, UUID.randomUUID().toString() + ".png", "drawing")
                //feedback
                if (imgSaved != null) {
                    val savedToast = Toast.makeText(applicationContext, "Drawing saved to Gallery!", Toast.LENGTH_SHORT)
                    savedToast.show()
                } else {
                    val unsavedToast = Toast.makeText(applicationContext, "Oops! Image could not be saved.", Toast.LENGTH_SHORT)
                    unsavedToast.show()
                }
//                myDrawView.destroyDrawingCache()
            }
        })
        saveDialog.setNegativeButton(
            "Cancel"
        )
        { dialog, which -> dialog.cancel() }
        saveDialog.show()
    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!

                uriImagePicked = fileUri
                finish()
                var intent = Intent(this,BackgroundChangeActivity::class.java)
                intent.putExtra("uriImagePicked",uriImagePicked.toString())
                startActivity(intent)

//                mProfileUri = fileUri
//                imgProfile.setImageURI(fileUri)
            }
            else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
                finish()
            }
        }

    /*for BackgroundRemove*/
    private val imageResult =
        registerForActivityResult(
            ActivityResultContracts.GetContent()
        ) { uri: Uri? ->
            uri?.let { uri ->
//                binding.img.clearColorFilter()
                uriImagePicked = uri
                finish()

                var intent = Intent(this,BackgroundChangeActivity::class.java)
                intent.putExtra("uriImagePicked",uriImagePicked.toString())
                startActivity(intent)

                binding.img.setImageURI(uri)
//                binding.img.clearColorFilter()
            }
        }
    private fun removeBg() {
//        binding.img.invalidate()
        BackgroundRemover.bitmapForProcessing(
//            binding.img.drawable.getBitmap(),
            MediaStore.Images.Media.getBitmap(this.contentResolver, uriImagePicked),
            true,
            object : OnBackgroundChangeListener {
                override fun onSuccess(bitmap: Bitmap) {
                    Toast.makeText(this@ManualRemoveActivity, "Success", Toast.LENGTH_SHORT)
                        .show()

//                    binding.img.setImageBitmap(bitmap)

//                    BitMapForMaualEdit =bitmap;

//                    setImageInView()

//                    previewFinalImage()

//                    finalSave()
                }

                override fun onFailed(exception: Exception) {
                    Toast.makeText(this@ManualRemoveActivity, "Error Occur", Toast.LENGTH_SHORT)
                        .show()
                }

            })
    }
    private fun finalSave() {

//        for any layout use to creat bitmap

        /*  binding.img.invalidate()
          binding.img.setDrawingCacheEnabled(true)
          val bitmap: Bitmap = binding.img.getDrawingCache()
          val root = Environment.getExternalStorageDirectory().toString()*/

        val bm = (binding.img.getDrawable() as BitmapDrawable).bitmap

//        File newDir = new File(root + "/Pictures/photo_editor");
        val newDir: File? = baseContext.getExternalFilesDir("photo_editor")
        if (newDir != null) {
            newDir.mkdir()
        }
        val photoName = String.format("frame%d.jpg", System.currentTimeMillis())
        val file = File(newDir, photoName)
        val s: String = file.getAbsolutePath()

        try {
            val out = FileOutputStream(file)
//            stickerView.invalidate()
            bm.compress(Bitmap.CompressFormat.PNG, 100, out)
            out.flush()
//            Toast.makeText(this, "Photo Saved", Toast.LENGTH_SHORT).show()
            out.close()

//            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//            intent.setData(Uri.fromFile(file));
//            sendBroadcast(intent);
//            isNotYetImageSaved = false
//            previewFinalImage(Uri.fromFile(file))
        } catch (e: java.lang.Exception) {
            Toast.makeText(this@ManualRemoveActivity, "Error while saving", Toast.LENGTH_SHORT)
                .show()
        }
    }
    private fun previewFinalImage(/*uriToPreview: Uri*/) {

//        val previewIntent = Intent(
//            this@BackGroundAutoActivity
//            MainActivity::class.java
//        )

        var intent = Intent(applicationContext, MainActivity::class.java)
//        intent.putExtra("remove", bitmap)
        intent.putExtra("uri", "uriToPreview.toString()")
        startActivity(intent)

    }

    /*for Manual BackGroundRemove*/
    private fun setImageInView() {
        //        try {
        // When an Image is picked
        var sampledSrcBitmap: Bitmap? = null

        // Get the Image from data
//            Uri selectedImage = data.getData();
//            Uri selectedImage = Uri.parse(imagePath);
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)


        // Get the cursor
//            Cursor cursor = getContentResolver().query(selectedImage,
//                    filePathColumn, null, null, null);
//            // Move to first row
//            cursor.moveToFirst();

//            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//            imgDecodableString = cursor.getString(columnIndex);
//            cursor.close();

//            mImageView = (ImageView) findViewById(R.id.mainLayout);
        mLayout = findViewById<View>(R.id.mainLayout) as RelativeLayout
        val targetW = mLayout!!.width
        val targetH = mLayout!!.height
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(imgDecodableString, options)
        mBitmap = BitmapFactory.decodeResource(resources, R.drawable.ks2)
        mDensity = resources.displayMetrics.density.toDouble()
        viewWidth = resources.displayMetrics.widthPixels
        viewHeight = resources.displayMetrics.heightPixels - actionBarHeight - bottombarHeight
        viewRatio = viewHeight.toDouble() / viewWidth.toDouble()
        bmRatio = mBitmap?.getHeight()!!.toDouble() / mBitmap?.getWidth()!!.toDouble()
        if (bmRatio < viewRatio) {
            bmWidth = viewWidth
            bmHeight = (viewWidth.toDouble() * (mBitmap?.getHeight()!!.toDouble() / mBitmap?.getWidth()!!
                .toDouble())).toInt()
        } else {
            bmHeight = viewHeight
            bmWidth = (viewHeight.toDouble() * (mBitmap?.getWidth()!!.toDouble() / mBitmap?.getHeight()!!
                .toDouble())).toInt()
        }

//            Uri imageUri = Uri.parse(imagePath);
        sampledSrcBitmap = null

            sampledSrcBitmap = BitMapForMaualEdit

        mBitmap = Bitmap.createScaledBitmap(
            sampledSrcBitmap!!,
            sampledSrcBitmap.width,
            sampledSrcBitmap.height,
            true
        )
        sampledSrcBitmap = null
        mHoverView = HoverView(this, mBitmap, bmWidth, bmHeight, viewWidth, viewHeight)
        mHoverView!!.layoutParams = RelativeLayout.LayoutParams(viewWidth, viewHeight)
        mLayout!!.addView(mHoverView)
        initButton()
    }

    fun initButton() {
        eraserMainButton = findViewById<View>(R.id.eraseButton) as Button
        eraserMainButton!!.setOnClickListener(this)
        magicWandMainButton = findViewById<View>(R.id.magicButton) as Button
        magicWandMainButton!!.setOnClickListener(this)
        mirrorButton = findViewById<View>(R.id.mirrorButton) as Button
        mirrorButton!!.setOnClickListener(this)
        positionButton = findViewById<View>(R.id.positionButton) as Button
        positionButton!!.setOnClickListener(this)
        eraserSubButton = findViewById<View>(R.id.erase_sub_button) as ImageView
        eraserSubButton!!.setOnClickListener(this)
        unEraserSubButton = findViewById<View>(R.id.unerase_sub_button) as ImageView
        unEraserSubButton!!.setOnClickListener(this)
        brushSize1Button = findViewById<View>(R.id.brush_size_1_button) as ImageView
        brushSize1Button!!.setOnClickListener(this)
        brushSize2Button = findViewById<View>(R.id.brush_size_2_button) as ImageView
        brushSize2Button!!.setOnClickListener(this)
        brushSize3Button = findViewById<View>(R.id.brush_size_3_button) as ImageView
        brushSize3Button!!.setOnClickListener(this)
        brushSize4Button = findViewById<View>(R.id.brush_size_4_button) as ImageView
        brushSize4Button!!.setOnClickListener(this)
        magicSeekbar = findViewById<View>(R.id.magic_seekbar) as SeekBar
        magicSeekbar!!.progress = 15
        magicSeekbar!!.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mHoverView!!.setMagicThreshold(seekBar.progress)
                if (mHoverView!!.mode == HoverView.MAGIC_MODE) mHoverView!!.magicEraseBitmap() else if (mHoverView!!.mode == HoverView.MAGIC_MODE_RESTORE) mHoverView!!.magicRestoreBitmap()
                mHoverView!!.invalidateView()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                /*mHoverView.setMagicThreshold(progress);
					if(mHoverView.getMode() == mHoverView.MAGIC_MODE)
						mHoverView.magicEraseBitmap();
					else if(mHoverView.getMode() == mHoverView.MAGIC_MODE_RESTORE)
						mHoverView.magicRestoreBitmap();
					mHoverView.invalidateView();*/
            }
        })
        magicRemoveButton = findViewById<View>(R.id.magic_remove_button) as ImageView
        magicRemoveButton!!.setOnClickListener(this)
        magicRestoreButton = findViewById<View>(R.id.magic_restore_button) as ImageView
        magicRestoreButton!!.setOnClickListener(this)
//        nextButton = findViewById<View>(R.id.nextButton) as Button
//        nextButton!!.setOnClickListener(this)
        undoButton = findViewById<View>(R.id.undoButton) as ImageView
        undoButton!!.setOnClickListener(this)
        redoButton = findViewById<View>(R.id.redoButton) as ImageView
        redoButton!!.setOnClickListener(this)
        updateRedoButton()
        eraserLayout = findViewById<View>(R.id.eraser_layout) as RelativeLayout
        magicLayout = findViewById<View>(R.id.magicWand_layout) as RelativeLayout
        eraserMainButton!!.isSelected = true
        colorButton = findViewById<View>(R.id.colorButton) as ImageView
        colorButton!!.setOnClickListener(this)
    }
    private fun getOrientationMatrix(path: String): Matrix? {
        val matrix = Matrix()
        val exif: ExifInterface
        try {
            exif = ExifInterface(path)
            val orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )
            when (orientation) {
                ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.setScale(-1f, 1f)
                ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
                ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
                    matrix.setRotate(180f)
                    matrix.postScale(-1f, 1f)
                }
                ExifInterface.ORIENTATION_TRANSPOSE -> {
                    matrix.setRotate(90f)
                    matrix.postScale(-1f, 1f)
                }
                ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
                ExifInterface.ORIENTATION_TRANSVERSE -> {
                    matrix.setRotate(-90f)
                    matrix.postScale(-1f, 1f)
                }
                ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(-90f)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return matrix
    }
    fun setBackGroundColor(color: Int) {
        when (color) {
            0 -> {
                mLayout!!.setBackgroundResource(R.drawable.bg)
                colorButton!!.setBackgroundResource(R.drawable.white_drawable)
            }
            1 -> {
                mLayout!!.setBackgroundColor(Color.WHITE)
                colorButton!!.setBackgroundResource(R.drawable.black_drawable)
            }
            2 -> {
                mLayout!!.setBackgroundColor(Color.BLACK)
                colorButton!!.setBackgroundResource(R.drawable.transparent_drawable)
            }
            else -> {}
        }
        currentColor = color
    }
    fun resetMainButtonState() {
        eraserMainButton!!.isSelected = false
        magicWandMainButton!!.isSelected = false
        mirrorButton!!.isSelected = false
        positionButton!!.isSelected = false
    }
    fun resetSubEraserButtonState() {
        eraserSubButton!!.isSelected = false
        unEraserSubButton!!.isSelected = false
    }
    fun resetSubMagicButtonState() {
        magicRemoveButton!!.isSelected = false
        magicRestoreButton!!.isSelected = false
    }
    fun resetBrushButtonState() {
        brushSize1Button!!.isSelected = false
        brushSize2Button!!.isSelected = false
        brushSize3Button!!.isSelected = false
        brushSize4Button!!.isSelected = false
    }
    fun updateUndoButton() {
        if (mHoverView!!.checkUndoEnable()) {
            undoButton!!.isEnabled = true
            undoButton!!.alpha = 1.0f
        } else {
            undoButton!!.isEnabled = false
            undoButton!!.alpha = 0.3f
        }
    }
    fun updateRedoButton() {
        if (mHoverView!!.checkRedoEnable()) {
            redoButton!!.isEnabled = true
            redoButton!!.alpha = 1.0f
        } else {
            redoButton!!.isEnabled = false
            redoButton!!.alpha = 0.3f
        }
    }
    override fun onClick(v: View) {
        updateUndoButton()
        updateRedoButton()
        when (v.id) {
            R.id.eraseButton -> {
                mHoverView!!.switchMode(HoverView.ERASE_MODE)
                if (eraserLayout!!.visibility == View.VISIBLE) {
                    eraserLayout!!.visibility = View.GONE
                } else {
                    eraserLayout!!.visibility = View.VISIBLE
                }
                magicLayout!!.visibility = View.GONE
                resetMainButtonState()
                resetSubEraserButtonState()
                eraserSubButton!!.isSelected = true
                eraserMainButton!!.isSelected = true
            }
            R.id.magicButton -> {
                mHoverView!!.switchMode(HoverView.MAGIC_MODE)
                if (magicLayout!!.visibility == View.VISIBLE) {
                    magicLayout!!.visibility = View.GONE
                } else {
                    magicLayout!!.visibility = View.VISIBLE
                }
                eraserLayout!!.visibility = View.GONE
                resetMainButtonState()
                resetSubMagicButtonState()
                resetSeekBar()
                magicRemoveButton!!.isSelected = true
                magicWandMainButton!!.isSelected = true
            }
            R.id.mirrorButton -> {
                findViewById<View>(R.id.eraser_layout).visibility = View.GONE
                findViewById<View>(R.id.magicWand_layout).visibility = View.GONE
                mHoverView!!.mirrorImage()
            }
            R.id.positionButton -> {
                mHoverView!!.switchMode(HoverView.MOVING_MODE)
                findViewById<View>(R.id.magicWand_layout).visibility = View.GONE
                findViewById<View>(R.id.eraser_layout).visibility = View.GONE
                resetMainButtonState()
                positionButton!!.isSelected = true
            }
            R.id.erase_sub_button -> {
                mHoverView!!.switchMode(HoverView.ERASE_MODE)
                resetSubEraserButtonState()
                eraserSubButton!!.isSelected = true
            }
            R.id.unerase_sub_button -> {
                mHoverView!!.switchMode(HoverView.UNERASE_MODE)
                resetSubEraserButtonState()
                unEraserSubButton!!.isSelected = true
            }
            R.id.brush_size_1_button -> {
                resetBrushButtonState()
                mHoverView!!.setEraseOffset(40)
                brushSize1Button!!.isSelected = true
            }
            R.id.brush_size_2_button -> {
                resetBrushButtonState()
                mHoverView!!.setEraseOffset(60)
                brushSize2Button!!.isSelected = true
            }
            R.id.brush_size_3_button -> {
                resetBrushButtonState()
                mHoverView!!.setEraseOffset(80)
                brushSize3Button!!.isSelected = true
            }
            R.id.brush_size_4_button -> {
                resetBrushButtonState()
                mHoverView!!.setEraseOffset(100)
                brushSize4Button!!.isSelected = true
            }
            R.id.magic_remove_button -> {
                resetSubMagicButtonState()
                magicRemoveButton!!.isSelected = true
                mHoverView!!.switchMode(HoverView.MAGIC_MODE)
                resetSeekBar()
            }
            R.id.magic_restore_button -> {
                resetSubMagicButtonState()
                magicRestoreButton!!.isSelected = true
                mHoverView!!.switchMode(HoverView.MAGIC_MODE_RESTORE)
                resetSeekBar()
            }
//            R.id.nextButton -> {
//                val intent = Intent(applicationContext, PositionActivity::class.java)
//                intent.putExtra("imagePath", mHoverView!!.save())
//                startActivity(intent)
//            }
            R.id.colorButton -> setBackGroundColor((currentColor + 1) % 3)
            R.id.undoButton -> {
                findViewById<View>(R.id.eraser_layout).visibility = View.GONE
                findViewById<View>(R.id.magicWand_layout).visibility = View.GONE
                mHoverView!!.undo()
                if (mHoverView!!.checkUndoEnable()) {
                    undoButton!!.isEnabled = true
                    undoButton!!.alpha = 1.0f
                } else {
                    undoButton!!.isEnabled = false
                    undoButton!!.alpha = 0.3f
                }
                updateRedoButton()
            }
            R.id.redoButton -> {
                findViewById<View>(R.id.eraser_layout).visibility = View.GONE
                findViewById<View>(R.id.magicWand_layout).visibility = View.GONE
                mHoverView!!.redo()
                updateUndoButton()
                updateRedoButton()
            }
        }
    }
    fun resetSeekBar() {
        magicSeekbar!!.progress = 0
        mHoverView!!.setMagicThreshold(0)
    }


    fun loadImagefromGallery(view: View?) {
        // Create intent to Open Image applications like Gallery, Google Photos
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //        try {
        // When an Image is picked
        var sampledSrcBitmap: Bitmap? = null
        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {

            // Get the Image from data
            val selectedImage = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            // Get the cursor
            val cursor = contentResolver.query(
                selectedImage!!,
                filePathColumn, null, null, null
            )
            // Move to first row
            cursor!!.moveToFirst()
            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            imgDecodableString = cursor.getString(columnIndex)
            cursor.close()
            //            mImageView = (ImageView) findViewById(R.id.mainLayout);
            mLayout = findViewById<View>(R.id.mainLayout) as RelativeLayout
            val targetW = mLayout!!.width
            val targetH = mLayout!!.height
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeFile(imgDecodableString, options)
            mBitmap = BitmapFactory.decodeResource(resources, R.drawable.ks2)
            mDensity = resources.displayMetrics.density.toDouble()
            viewWidth = resources.displayMetrics.widthPixels
            viewHeight = resources.displayMetrics.heightPixels - actionBarHeight - bottombarHeight
            viewRatio = viewHeight.toDouble() / viewWidth.toDouble()
            bmRatio = mBitmap!!.getHeight().toDouble() / mBitmap!!.getWidth().toDouble()
            if (bmRatio < viewRatio) {
                bmWidth = viewWidth
                bmHeight =
                    (viewWidth.toDouble() * (mBitmap!!.getHeight().toDouble() / mBitmap!!.getWidth()
                        .toDouble())).toInt()
            } else {
                bmHeight = viewHeight
                bmWidth =
                    (viewHeight.toDouble() * (mBitmap!!.getWidth().toDouble() / mBitmap!!.getHeight()
                        .toDouble())).toInt()
            }


//            int srcWidth = options.outWidth;
//            int srcHeight = options.outHeight;
//
//// Only scale if the source is big enough. This code is just trying to fit a image into a certain width.
//            if (targetW > srcWidth)
//                targetW = srcWidth;
//
//
//// Calculate the correct inSampleSize/scale value. This helps reduce memory use. It should be a power of 2
//
//            int inSampleSize = 1;
//            while (srcWidth / 2 > targetW) {
//                srcWidth /= 2;
//                srcHeight /= 2;
//                inSampleSize *= 2;
//            }
//
//            float desiredScale = (float) targetW / srcWidth;
//
//// Decode with inSampleSize
//            options.inJustDecodeBounds = false;
//            options.inDither = false;
//            options.inSampleSize = inSampleSize;
//            options.inScaled = false;
//            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            val imageUri = data.data
            sampledSrcBitmap = null
            try {
                sampledSrcBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
            } catch (e: IOException) {
                e.printStackTrace()
            }


//                Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(imgDecodableString, options);

// Resize
//            Matrix matrix = new Matrix();
//            matrix.postScale(desiredScale, desiredScale);
////                sampledSrcBitmap.getWidth();
//            Log.d("kdjfkf", "onActivityResult: Matrix:  " + matrix.postScale(desiredScale, desiredScale));

//                bitmap = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
            mBitmap = Bitmap.createScaledBitmap(
                sampledSrcBitmap!!,
                sampledSrcBitmap.width,
                sampledSrcBitmap.height,
                true
            )
            //                    mBitmap = Bitmap.createScaledBitmap(mBitmap, bmWidth, bmHeight, false);
            sampledSrcBitmap = null

//            mImageView.setImageBitmap(mBitmap);
//            mLayout.addView(mHoverView);
            mHoverView = HoverView(this, mBitmap, bmWidth, bmHeight, viewWidth, viewHeight)
            //        mHoverView.setLayoutParams(new RelativeLayout.LayoutParams(viewWidth, viewHeight));

//            mHoverView = new HoverView(this, mBitmap, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), targetW, targetH);
            mHoverView!!.layoutParams = RelativeLayout.LayoutParams(viewWidth, viewHeight)
            mLayout!!.addView(mHoverView)
            initButton()
        } else {
            if (requestCode != REQUEST_IMAGE_CROP) {
//                Toast.makeText(
//                    this, "You haven't picked Image",
//                    Toast.LENGTH_LONG
//                ).show()
            }
        }
//        } catch (Exception e) {
//            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
//                    .show();
//            Log.e("kdjfkf", "onActivityResult Exception : "+e.getMessage());
//        }

//        if (requestCode == REQUEST_IMAGE_CROP && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            //get the cropped bitmap
//            croppedBitmap = extras.getParcelable("data");
//            ImageView mImageView = (ImageView) findViewById(R.id.mainImg);
//            mImageView.setImageBitmap(croppedBitmap);
//
//        }
    }
}
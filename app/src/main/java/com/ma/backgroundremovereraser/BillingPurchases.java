package com.ma.backgroundremovereraser;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.QueryPurchasesParams;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;

public class BillingPurchases {
    public static BillingClient billingClient;
    public static String ONE_WEEK, FOUR_WEEK, THREE_MONTH, LIFETIME;
    public static boolean isPurchase = false, isBpClientReady = false;

    public static Context contextGlobal;
    public static SkuDetails week1Detail = null, week4detail = null, month3Detail = null, lifetimeDetail = null;
    public static PurchasesUpdatedListener purchasesUpdatedListener = new PurchasesUpdatedListener() {
        @Override
        public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<com.android.billingclient.api.Purchase> list) {
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
                for (com.android.billingclient.api.Purchase purchase : list) {
                    handlePurchase(purchase, contextGlobal);
                }

            } else {
                try {
                    if (!billingResult.getDebugMessage().isEmpty()) {
                        Toast.makeText(contextGlobal, billingResult.getDebugMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    };

    public static void updatePurchases(List<Purchase> list) {
        List<Purchase> updatedPurchaseList;
        updatedPurchaseList = list;
        if (updatedPurchaseList.size() > 0) {
            for (Purchase purchase : updatedPurchaseList) {
                if (purchase.getSkus().equals(LIFETIME)) {
                    isPurchase = true;
                    break;
                } else {
                    isPurchase = false;
                }
            }
        } else {
            isPurchase = false;
        }
    }

    public static BillingClientStateListener billingClientStateListener = new BillingClientStateListener() {
        @Override
        public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                isBpClientReady = true;
                get_owned_purchases();
                query_subscriptions();
                query_products();
            }
            //   test in App for ads by un-commenting below line
//            isPurchase=true;
        }

        @Override
        public void onBillingServiceDisconnected() {

        }
    };

    public static void setup_billing_client(Context context) {
        contextGlobal = context;

        LIFETIME = context.getString(R.string.lifeTime);

        billingClient = BillingClient.newBuilder(context)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build();

        if (!billingClient.isReady()) {
            billingClient.startConnection(billingClientStateListener);
        }
    }

    public static void launch_billing_flow(Activity activity, SkuDetails skuDetails) {
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)
                .build();
        int responseCode = billingClient.launchBillingFlow(activity, billingFlowParams).getResponseCode();
        if (responseCode == BillingClient.BillingResponseCode.OK) {

        } else if (responseCode == BillingClient.BillingResponseCode.BILLING_UNAVAILABLE) {
            Toast.makeText(activity, "Billing Unavailable!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, "Something went wrong!", Toast.LENGTH_SHORT).show();
        }

    }

    public static void query_subscriptions() {
        List<String> skuListInApp = new ArrayList<>();
        skuListInApp.add(ONE_WEEK);
        skuListInApp.add(FOUR_WEEK);
        skuListInApp.add(THREE_MONTH);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuListInApp).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        // Process the result.
                        //due to skuDetailsList null so its crashes..try catch is solution
                        try {
                            if (skuDetailsList==null){
                                Log.d("skuDetailsList","null");
                            }else {
                                if (skuDetailsList.size() > 0) {
                                    for (SkuDetails skuDetails : skuDetailsList) {
                                        Log.d("inapplist", skuDetails.getTitle());
                                        if (skuDetails.getSku().equals(ONE_WEEK)) {
                                            week1Detail = skuDetails;
                                        } else if (skuDetails.getSku().equals(FOUR_WEEK)) {
                                            week4detail = skuDetails;
                                        } else if (skuDetails.getSku().equals(THREE_MONTH)) {
                                            month3Detail = skuDetails;
                                        }
                                    }
                                } else {
                                    week1Detail = null;
                                    week4detail = null;
                                    month3Detail = null;
                                }
                            }
                        }catch (Exception ex){
                            Log.d("payment", "onSkuDetailsResponse: null");

                        }

                    }
                }
        );

    }

    public static void query_products() {
        List<String> skuListInApp = new ArrayList<>();
        skuListInApp.add(LIFETIME);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuListInApp).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        //due to skuDetailsList null so its crashes..try catch is solution

                        try {
                            if (skuDetailsList==null){
                                Log.d("skuDetailsList","null");
                            }else {
                                if (skuDetailsList.size() > 0) {
                                    for (SkuDetails skuDetails : skuDetailsList) {
                                        Log.d("inapplist", skuDetails.getTitle());
                                        if (skuDetails.getSku().equals(LIFETIME)) {
                                            lifetimeDetail = skuDetails;
                                        }

                                    }
                                } else {
                                    lifetimeDetail = null;
                                }
                            }
                        }catch (Exception ex){
                            Log.d("payment", "onSkuDetailsResponse: null");

                        }
                    }

                });

    }

    public static void get_owned_purchases() {
        List<com.android.billingclient.api.Purchase> purchasesInApp, purchasesSub;
        billingClient.queryPurchasesAsync(QueryPurchasesParams.newBuilder().setProductType(BillingClient.ProductType.INAPP).build(), new PurchasesResponseListener() {
            @Override
            public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> purchaseList) {

                if (purchaseList != null) {
                    for (Purchase purchase : purchaseList) {
                        if (purchase.getSkus().equals(LIFETIME)) {
                            isPurchase = true;
                            break;
                        } else {
                            isPurchase = false;
                        }
                    }
                } else {
                    isPurchase = false;
                }
            }
        });


    }

    public static void onPurchasedItemDialog(Context context) {
        get_owned_purchases();
    }

    public static void handlePurchase(Purchase purchase, Context context) {
        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener = new AcknowledgePurchaseResponseListener() {
                    @Override
                    public void onAcknowledgePurchaseResponse(@NonNull BillingResult billingResult) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            // Handle the success of the consume operation.
                            onPurchasedItemDialog(context);
                        }
                    }
                };
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, acknowledgePurchaseResponseListener);
            } else {
                onPurchasedItemDialog(context);
            }
        }
    }

}
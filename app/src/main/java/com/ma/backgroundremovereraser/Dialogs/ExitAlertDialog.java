package com.ma.backgroundremovereraser.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.ma.backgroundremovereraser.R;

import java.util.Objects;


public class ExitAlertDialog extends Dialog implements View.OnClickListener {
    Context mContext;
    public ExitAlertDialog(@NonNull Context context) {
        super(context);
        this.mContext=context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dlg_exit);
        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bindViews();
    }
    private void bindViews() {

        RelativeLayout btnYes = findViewById(R.id.btn_yes);
        RelativeLayout btnNo = findViewById(R.id.btn_no);

        btnNo.setOnClickListener(this);
        btnYes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                ((Activity) mContext).finishAffinity();
                break;
            case R.id.btn_no:
                dismiss();
                break;


        }
    }
}


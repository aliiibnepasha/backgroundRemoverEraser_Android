package com.ma.backgroundremovereraser.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ma.backgroundremovereraser.R;
import com.ma.backgroundremovereraser.activities.MainActivity;


import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;


public class SaveScreenDialog extends Dialog implements View.OnClickListener {

    Bitmap imBitmap;
    Context mContext;


    public SaveScreenDialog(@NonNull Context context, Bitmap bitmap) {
        super(context, R.style.full_screen_dialog);

        this.imBitmap=bitmap;
        this.mContext=context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_dialog_save);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        bindViews();
    }
    private void bindViews() {

        ImageView ivImage = findViewById(R.id.ivImage);
        ImageView iv_Home = findViewById(R.id.iv_Home);
        TextView tv_Back = findViewById(R.id.tv_Back);
        TextView tv_ImageName = findViewById(R.id.tv_ImageName);
        LinearLayout save = findViewById(R.id.save);
        LinearLayout ll_rateus = findViewById(R.id.ll_rateus);
        LinearLayout llSaveFile = findViewById(R.id.llSaveFile);
        LinearLayout llExportSettings = findViewById(R.id.llExportSettings);
        LinearLayout ll_moreApp = findViewById(R.id.ll_moreApp);
        LinearLayout ll_whatsapp = findViewById(R.id.ll_whatsapp);
        LinearLayout ll_copy = findViewById(R.id.ll_copy);
        LinearLayout ll_ourApps = findViewById(R.id.ll_ourApps);

        ivImage.setImageBitmap(imBitmap);

        tv_Back.setOnClickListener(this);
        iv_Home.setOnClickListener(this);
        save.setOnClickListener(this);
        ll_rateus.setOnClickListener(this);
        llSaveFile.setOnClickListener(this);
        llExportSettings.setOnClickListener(this);
        ll_moreApp.setOnClickListener(this);
        ll_whatsapp.setOnClickListener(this);
        ll_copy.setOnClickListener(this);
        ll_ourApps.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_Back:
                dismiss();

                break;
            case R.id.iv_Home:
                Intent intent = new Intent(mContext, MainActivity.class);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
//                mContext.finish();
                break;
            case R.id.llSaveFile:
            case R.id.save:
                finalSave();
                Intent intent2 = new Intent(mContext, MainActivity.class);
                mContext.startActivity(intent2);
                ((Activity) mContext).finish();
                break;
            case R.id.ll_rateus:
                rateUs();
                break;
            case R.id.llExportSettings:
                ExportAlertDialog exportAlertDialog=new ExportAlertDialog(mContext);
                exportAlertDialog.show();

                break;
            case R.id.ll_whatsapp:
                openWebView("https://web.whatsapp.com/");
                break;
            case R.id.ll_copy:

                break;
            case R.id.ll_ourApps:
                moreApp();
                break;
            case R.id.ll_moreApp:
                moreApp();
                break;

        }
    }

    private void finalSave() {
        String root = Environment.getExternalStorageDirectory().toString();
        File newDir = new File(root + "/Pictures/bgremover");
        newDir.mkdir();
        Random gen = new Random();
        int n = 10000;
        n = gen.nextInt(n);
        String photoName = String.format("%d.png", System.currentTimeMillis());
        File file = new File(newDir, photoName);
        String s = file.getAbsolutePath();
        Log.d("savedimagepath", "saved Image Path" + s);
        try {
            FileOutputStream out = new FileOutputStream(file);

            imBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
//            imageUriPreviewSendImage = Uri.fromFile(new File(file.getAbsolutePath()));
            out.flush();
            Toast.makeText(mContext, "Photo Saved", Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext, "out " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            Log.e("savedimagepath", "finalSave: path is : " + file.getAbsolutePath());
            out.close();

            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(file));
            mContext.sendBroadcast(intent);

        } catch (Exception e) {

        }

    }


    private void moreApp() {
        final String appPackageName = mContext.getPackageName(); // getPackageName() from Context or Activity object
        try {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void rateUs() {
        Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            mContext.startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    private void openWebView(String strLink) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(strLink));
        mContext.startActivity(i);
    }
}



package com.ma.backgroundremovereraser.Dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.ma.backgroundremovereraser.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;



public class ExportAlertDialog extends BottomSheetDialog implements View.OnClickListener, BottomSheetDialog.OnShowListener  {



    public ExportAlertDialog(@NonNull Context context) {
//        super(context);

        super(context, R.style.SubMenuBottomSheetDialogTheme);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dlg_export);
        this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    private void bindViews() {


    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public void onShow(DialogInterface dialog) {
        BottomSheetDialog sheetDialog = (BottomSheetDialog) dialog;

        FrameLayout bottomSheet = (FrameLayout) sheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

        sheetDialog.setCancelable(false);
        sheetDialog.setCanceledOnTouchOutside(true);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setHideable(false);

    }

}


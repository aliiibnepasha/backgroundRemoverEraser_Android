package com.ma.backgroundremovereraser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ma.backgroundremovereraser.R;

import java.util.List;


public class GradientsAdapter extends RecyclerView.Adapter<GradientsAdapter.ViewHolder> {

    private List<Integer> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context mContext;

    // data is passed into the constructor
    public GradientsAdapter(Context context, List<Integer> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext=context;
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_gradients, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int color = mData.get(position);

//        holder.ivItemGradient.setBackground(mContext.getResources().getDrawable(color));
//        holder.ivItemGradient.setImageDrawable(mContext.getResources().getDrawable(color));
//        holder.ivItemGradient.setImageResource(color);
//        holder.ivItemGradient.setImageDrawable(mContext.getResources().getDrawable(color));

        Glide.with(mContext).load(mContext.getResources().getDrawable(color)).into(holder.ivItemGradient);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivItemGradient;

        ViewHolder(View itemView) {
            super(itemView);
            ivItemGradient = itemView.findViewById(R.id.ivItemGradient);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
//    int getItem(int id) {
//        return mData.get(id);
//    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
package com.ma.backgroundremovereraser.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.ma.backgroundremovereraser.R;

import java.util.List;


public class ColorsAdapter extends RecyclerView.Adapter<ColorsAdapter.ViewHolder> {

    private List<String> mData;
    private LayoutInflater mInflater;
    private ColorItemClickListener mClickListener;
    Context mContext;

    // data is passed into the constructor
    public ColorsAdapter(Context context, List<String> data,ColorItemClickListener mClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext=context;
        this.mData = data;
        this.mClickListener = mClickListener;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_colors, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String color = mData.get(position);

//        holder.ivColor.setBackgroundColor(mContext.getResources().getColor(color));
        holder.ivColor.setBackgroundColor(Color.parseColor(color));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mClickListener.onItemClick(v,holder.getAdapterPosition());
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        View ivColor;

        ViewHolder(View itemView) {
            super(itemView);
            ivColor = itemView.findViewById(R.id.ivItemColor);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
//    int getItem(int id) {
//        return mData.get(id);
//    }

    // allows clicks events to be caught
    void setClickListener(ColorItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ColorItemClickListener {
        void onItemClick(View view, int position);
    }
}
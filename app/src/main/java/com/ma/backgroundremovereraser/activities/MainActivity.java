package com.ma.backgroundremovereraser.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ma.backgroundremovereraser.BuildConfig;
import com.ma.backgroundremovereraser.Dialogs.ExitAlertDialog;
import com.ma.backgroundremovereraser.R;
import com.ma.backgroundremovereraser.bgremover.ManualRemoveActivity;
import com.ma.backgroundremovereraser.databinding.ActivityMainBinding;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout llGallery,llCamera;
    RelativeLayout rlDrawerCancel,rlResolution;

    DrawerLayout drawerLayout;
    NavigationView nav_view;

    ImageView ivDrawerNav;


    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        if (InitApplication.getInstance().isNightModeEnabled()) {
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//        } else {
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//        }
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);

        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        llGallery=findViewById(R.id.gallery_ll);
        llCamera=findViewById(R.id.camera_ll);
        nav_view=findViewById(R.id.nav_view);
        drawerLayout= findViewById(R.id.drawer_layout);
        ivDrawerNav= findViewById(R.id.drawer_nav_iv);
        rlDrawerCancel= findViewById(R.id.drawer_cancel_rl);
        rlResolution= findViewById(R.id.resolution_rl);


//        llGallery.setOnClickListener(this);
//        llCamera.setOnClickListener(this);
//        ivDrawerNav.setOnClickListener(this);
//        rlDrawerCancel.setOnClickListener(this);
//        rlResolution.setOnClickListener(this);

        binding.galleryLl.setOnClickListener(this);
        binding.cameraLl.setOnClickListener(this);
        binding.drawerNavIv.setOnClickListener(this);

        binding.drawer.drawerCancelRl.setOnClickListener(this);
        binding.drawer.resolutionRl.setOnClickListener(this);
        binding.drawer.llRateus.setOnClickListener(this);
        binding.drawer.llFeedback.setOnClickListener(this);
        binding.drawer.llInstagram.setOnClickListener(this);
        binding.drawer.llMoreApp.setOnClickListener(this);
        binding.drawer.llPrivacy.setOnClickListener(this);
        binding.drawer.llShare.setOnClickListener(this);
        binding.drawer.llTermuse.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gallery_ll:
                Toast.makeText(this, "Gallery", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(MainActivity.this,SaveImageActivity.class));
//                startActivity(new Intent(MainActivity.this, BackgroundChangeActivity.class));
//                startActivity(new Intent(MainActivity.this, BackgroundRemoveActivity.class));
                startActivity(new Intent(MainActivity.this, ManualRemoveActivity.class));
                break;
            case R.id.camera_ll:
                startActivity(new Intent(MainActivity.this,PurchaseActivity.class));
                Toast.makeText(this, "Camera", Toast.LENGTH_SHORT).show();
                break;
            case R.id.drawer_nav_iv:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.drawer_cancel_rl:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.resolution_rl:
               ResolutionAlertDialog resolutionAlertDialog=new ResolutionAlertDialog(MainActivity.this);
               resolutionAlertDialog.show();

                break;

            case R.id.ll_feedback:
            case R.id.ll_instagram:
            case R.id.ll_privacy:
            case R.id.ll_termuse:
                openWebView("https://www.instagram.com/");
                break;
            case R.id.ll_moreApp:
                moreApp();
                break;
            case R.id.ll_rateus:
                rateUs();
                break;
            case R.id.ll_share:
                shareApp();
                break;

        }

    }

    private void moreApp() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void shareApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
            String shareMessage= "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch(Exception e) {
            //e.toString();
        }
    }

    private void rateUs() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    private void openWebView(String strLink) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(strLink));
        startActivity(i);
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }else {
            ExitAlertDialog exitAlertDialog=new ExitAlertDialog(MainActivity.this);
            exitAlertDialog.show();

            //super.onBackPressed();
        }

    }
}
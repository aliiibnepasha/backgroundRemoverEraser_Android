package com.ma.backgroundremovereraser.activities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;



import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;

import com.ma.backgroundremovereraser.R;

import java.util.Objects;


public class ResolutionAlertDialog extends Dialog implements View.OnClickListener {



    public ResolutionAlertDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);

        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dlg_resolution);
        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bindViews();
    }
    private void bindViews() {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


        }
    }
}


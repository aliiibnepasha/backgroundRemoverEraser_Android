package com.ma.backgroundremovereraser.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.ma.backgroundremovereraser.Dialogs.SaveScreenDialog;
import com.ma.backgroundremovereraser.helpers.OnBackgroundChangeListener;
import com.ma.backgroundremovereraser.R;
import com.ma.backgroundremovereraser.adapters.ColorsAdapter;
import com.ma.backgroundremovereraser.adapters.GradientsAdapter;
import com.ma.backgroundremovereraser.adapters.PatternsAdapter;
import com.ma.backgroundremovereraser.bgremover.BackgroundRemover;
import com.ma.backgroundremovereraser.helpers.ListHelper;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.StickerView;

import java.io.IOException;
import java.util.List;

public class BackgroundChangeActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rvColors, rvGradients, rvPatterns;
    ColorsAdapter colorsAdapter;
    GradientsAdapter gradientsAdapter;
    PatternsAdapter patternsAdapter;

    ConstraintLayout clImageAsBackgroundOption, clColorAsBackgroundOption, clGradientAsBackgroundOption, clPatternAsBackgroundOption;

    LinearLayout llImageOptionsChild, llColorOptionsChild, llGradientOptionsChild, llPatternOptionsChild;
    LinearLayout llPatternAsBackground, llGradientAsBackground, llColorAsBackground, llImageAsBackground;

    ImageView ivMainImageBg, clSaveOption;
    ConstraintLayout clImage;
    View ivMainViewColors;
    StickerView stickerView;
    private DrawableSticker drawableSticker;
    private Bitmap defaultPickedImageBitmap, currentImageBitmap, temporarySaveBitmap;

    private boolean isEditingBitmapHasChanges = false;
    private SeekBar seekbarBrightness;

    LinearLayout llImageBlur, llImageBrightness, llImageContrast, llImageExposure, llImageVibrance, llImageSaturation, llImageHighlight, llImageShadow, llImageFade;

    int progbrightness = 1000, progContrast = 1000, progSaturation = 1000;
    private int Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 0;
    private String TAG = "changebg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background_change);


        initializeViews();

        setClickListeners();

        getUriAndRemoveAutoBG();

        setupRecycler();
    }


    private void initializeViews() {
        ivMainImageBg = findViewById(R.id.ivMainImage);
        ivMainViewColors = findViewById(R.id.ivMainViewColors);
        clImage = findViewById(R.id.clImage);

        rvColors = findViewById(R.id.rvColors);
        rvGradients = findViewById(R.id.rvGradients);
        rvPatterns = findViewById(R.id.rvPatterns);
        stickerView = findViewById(R.id.stickerView);

        clImageAsBackgroundOption = findViewById(R.id.clImageAsBackgroundOption);
        clColorAsBackgroundOption = findViewById(R.id.clColorAsBackgroundOption);
        clGradientAsBackgroundOption = findViewById(R.id.clGradientAsBackgroundOption);
        clPatternAsBackgroundOption = findViewById(R.id.clPatternAsBackgroundOption);
        clSaveOption = findViewById(R.id.clSaveOption);
        llPatternAsBackground = findViewById(R.id.llPatternAsBackground);
        llGradientAsBackground = findViewById(R.id.llGradientAsBackground);
        llImageAsBackground = findViewById(R.id.llImageAsBackground);
        llColorAsBackground = findViewById(R.id.llColorAsBackground);

        llImageBlur = findViewById(R.id.llImageBlur);
        llImageBrightness = findViewById(R.id.llImageBrightness);
        llImageContrast = findViewById(R.id.llImageContrast);
        llImageExposure = findViewById(R.id.llImageExposure);
        llImageVibrance = findViewById(R.id.llImageVibrance);
        llImageSaturation = findViewById(R.id.llImageSaturation);
        llImageHighlight = findViewById(R.id.llImageHighlight);
        llImageShadow = findViewById(R.id.llImageShadow);
        llImageFade = findViewById(R.id.llImageFade);


        //content
        llImageOptionsChild = findViewById(R.id.llImageOptionsChild);
        llColorOptionsChild = findViewById(R.id.llColorOptionsChild);
        llGradientOptionsChild = findViewById(R.id.llGradientOptionsChild);
        llPatternOptionsChild = findViewById(R.id.llPatternOptionsChild);

        //seekbar
        seekbarBrightness = findViewById(R.id.seekbarBrightness);
    }

    private void setClickListeners() {
        clImageAsBackgroundOption.setOnClickListener(this);
        clColorAsBackgroundOption.setOnClickListener(this);
        clGradientAsBackgroundOption.setOnClickListener(this);
        clPatternAsBackgroundOption.setOnClickListener(this);
        clSaveOption.setOnClickListener(this);

        llImageOptionsChild.setOnClickListener(this);
        llColorOptionsChild.setOnClickListener(this);
        llGradientOptionsChild.setOnClickListener(this);
        llPatternOptionsChild.setOnClickListener(this);

        llImageBlur.setOnClickListener(this);
        llImageBrightness.setOnClickListener(this);
        llImageContrast.setOnClickListener(this);
        llImageExposure.setOnClickListener(this);
        llImageVibrance.setOnClickListener(this);
        llImageSaturation.setOnClickListener(this);
        llImageHighlight.setOnClickListener(this);
        llImageShadow.setOnClickListener(this);
        llImageFade.setOnClickListener(this);

        seekbarBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                switch (Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8) {
                    case 0:
                        //Blur was Selected
//                        isEditingBitmapHasChanges = true;
                        break;
                    case 1:
                        //Brightness was Selected
                        isEditingBitmapHasChanges = true;
                        // for brightness
                        progbrightness = progress;
                        float value = progress - (seekBar.getMax() / 2);
                        temporarySaveBitmap = brightBitmap(currentImageBitmap, value / 10f);
                        drawableSticker = new DrawableSticker(new BitmapDrawable(getResources(),
                                temporarySaveBitmap));
                        stickerView.replace(drawableSticker);
                        break;
                    case 2:
                        //Contrast was Selected
                        isEditingBitmapHasChanges = true;
                        //for contrast
                        progContrast = progress;
                        float value2 = progress / 100;
//                value = progress - (50);
                        temporarySaveBitmap = contrastBitmap(currentImageBitmap, value2 / 10f);
                        drawableSticker = new DrawableSticker(new BitmapDrawable(getResources(),
                                temporarySaveBitmap));
                        stickerView.replace(drawableSticker);
                        break;
                    case 3:
                        //Exposure was Selected
//                        isEditingBitmapHasChanges = true;
                        break;
                    case 4:
                        //Vibrance was Selected
//                        isEditingBitmapHasChanges = true;
                        break;
                    case 5:
                        //Saturation was Selected
                        isEditingBitmapHasChanges = true;
                        //for saturation

                        progSaturation = progress;
                        float values = progress / 20;
//                value = progress - (50);
                        temporarySaveBitmap = saturationBitmap(currentImageBitmap, values / 10f);
                        drawableSticker = new DrawableSticker(new BitmapDrawable(getResources(),
                                temporarySaveBitmap));
                        stickerView.replace(drawableSticker);
                        break;
                    case 6:
                        //Highlights was Selected
//                        isEditingBitmapHasChanges = true;
                        break;
                    case 8:
                        //Fade was Selected
//                        isEditingBitmapHasChanges = true;
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void getUriAndRemoveAutoBG() {

        try {
            Uri pickedUri = Uri.parse(getIntent().getStringExtra("uriImagePicked"));


            BackgroundRemover.bitmapForProcessing(
                    MediaStore.Images.Media.getBitmap(this.getContentResolver(), pickedUri),
                    true, new OnBackgroundChangeListener() {
                        @Override
                        public void onSuccess(@NonNull Bitmap bitmap) {

                            Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
                            // Instantiate a canvas and prepare it to paint to the new bitmap
                            Canvas canvas = new Canvas(newBitmap);
                            // Paint it white (or whatever color you want)
                            canvas.drawColor(Color.WHITE);
                            // Draw the old bitmap ontop of the new white one
                            canvas.drawBitmap(bitmap, 0, 0, null);

                            defaultPickedImageBitmap = newBitmap;
                            currentImageBitmap = newBitmap;
                            drawableSticker = new DrawableSticker(new BitmapDrawable(getResources(), currentImageBitmap));
                            stickerView.addSticker(drawableSticker);
                        }

                        @Override
                        public void onFailed(@NonNull Exception exception) {

                        }
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setupRecycler() {
        //setup lists
        List<String> colorslist = ListHelper.getColorsList();

        // set up the COLORS  RecyclerView
        rvColors.setLayoutManager(new GridLayoutManager(this, 10));

        rvColors.setAdapter(new ColorsAdapter(this, colorslist, new ColorsAdapter.ColorItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                Log.e(TAG, "COLORRRR onItemClick : position " + position);
                Log.e(TAG, "COLORRRR onItemClick : colorr " + colorslist.get(position));
                ivMainViewColors.setBackgroundColor(Color.parseColor(colorslist.get(position)));
            }
        }));


        // set up the GRADIENTS  RecyclerView
        rvGradients.setLayoutManager(new GridLayoutManager(this, 10));
        gradientsAdapter = new GradientsAdapter(this, ListHelper.getGradientsList());
        rvGradients.setAdapter(gradientsAdapter);


        // set up the PATTERNS  RecyclerView
        rvPatterns.setLayoutManager(new GridLayoutManager(this, 10));
        patternsAdapter = new PatternsAdapter(this, ListHelper.getPatternsList());
        rvPatterns.setAdapter(patternsAdapter);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.clImageAsBackgroundOption:
                llImageOptionsChild.setVisibility(View.VISIBLE);
                llColorOptionsChild.setVisibility(View.INVISIBLE);
                llGradientOptionsChild.setVisibility(View.INVISIBLE);
                llPatternOptionsChild.setVisibility(View.INVISIBLE);

                llPatternAsBackground.setVisibility(View.INVISIBLE);
                llGradientAsBackground.setVisibility(View.INVISIBLE);
                llImageAsBackground.setVisibility(View.VISIBLE);
                llColorAsBackground.setVisibility(View.INVISIBLE);

                break;
            case R.id.clColorAsBackgroundOption:
                llImageOptionsChild.setVisibility(View.INVISIBLE);
                llColorOptionsChild.setVisibility(View.VISIBLE);
                llGradientOptionsChild.setVisibility(View.INVISIBLE);
                llPatternOptionsChild.setVisibility(View.INVISIBLE);

                llPatternAsBackground.setVisibility(View.INVISIBLE);
                llGradientAsBackground.setVisibility(View.INVISIBLE);
                llImageAsBackground.setVisibility(View.INVISIBLE);
                llColorAsBackground.setVisibility(View.VISIBLE);
                break;
            case R.id.clGradientAsBackgroundOption:

                llImageOptionsChild.setVisibility(View.INVISIBLE);
                llColorOptionsChild.setVisibility(View.INVISIBLE);
                llGradientOptionsChild.setVisibility(View.VISIBLE);
                llPatternOptionsChild.setVisibility(View.INVISIBLE);

                llPatternAsBackground.setVisibility(View.INVISIBLE);
                llGradientAsBackground.setVisibility(View.VISIBLE);
                llImageAsBackground.setVisibility(View.INVISIBLE);
                llColorAsBackground.setVisibility(View.INVISIBLE);
                break;
            case R.id.clPatternAsBackgroundOption:

                llImageOptionsChild.setVisibility(View.INVISIBLE);
                llColorOptionsChild.setVisibility(View.INVISIBLE);
                llGradientOptionsChild.setVisibility(View.INVISIBLE);
                llPatternOptionsChild.setVisibility(View.VISIBLE);

                llPatternAsBackground.setVisibility(View.VISIBLE);
                llGradientAsBackground.setVisibility(View.INVISIBLE);
                llImageAsBackground.setVisibility(View.INVISIBLE);
                llColorAsBackground.setVisibility(View.INVISIBLE);
                break;
            case R.id.llImageBlur:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 0;
                checkIfBitmapHasChangesThenUpdateIt();
                break;
            case R.id.llImageBrightness:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 1;
                checkIfBitmapHasChangesThenUpdateIt();
                seekbarBrightness.setProgress(progbrightness);
                break;
            case R.id.llImageContrast:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 2;
                checkIfBitmapHasChangesThenUpdateIt();
                seekbarBrightness.setProgress(progContrast);
                break;
            case R.id.llImageExposure:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 3;
                checkIfBitmapHasChangesThenUpdateIt();
                break;
            case R.id.llImageVibrance:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 4;
                checkIfBitmapHasChangesThenUpdateIt();
                break;
            case R.id.llImageSaturation:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 5;
                checkIfBitmapHasChangesThenUpdateIt();
                seekbarBrightness.setProgress(progSaturation);
                break;
            case R.id.llImageHighlight:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 6;
                checkIfBitmapHasChangesThenUpdateIt();
                break;
            case R.id.llImageShadow:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 7;
                checkIfBitmapHasChangesThenUpdateIt();
                break;
            case R.id.llImageFade:
                Blur0Bright1Cont2Exp3Vib4Sat5Hi6Sh7Fde8 = 8;
                checkIfBitmapHasChangesThenUpdateIt();
                break;
            case R.id.clSaveOption:

                stickerView.invalidate();
                clImage.setDrawingCacheEnabled(true);
                Bitmap bitmap = clImage.getDrawingCache();

                SaveScreenDialog saveScreenDialog = new SaveScreenDialog(BackgroundChangeActivity.this, bitmap);
                saveScreenDialog.show();
                saveScreenDialog.setCancelable(false);

                break;


        }
    }

    private void checkIfBitmapHasChangesThenUpdateIt() {
        if (isEditingBitmapHasChanges && temporarySaveBitmap!=null) {
            currentImageBitmap = temporarySaveBitmap;
            isEditingBitmapHasChanges = false;
        }
    }


    public Bitmap brightBitmap(Bitmap bitmap, float brightness) {
        float[] colorTransform = {
                1, 0, 0, 0, brightness,
                0, 1, 0, 0, brightness,
                0, 0, 1, 0, brightness,
                0, 0, 0, 1, 0};

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0f);
        colorMatrix.set(colorTransform);

        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);


        Bitmap resultBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, paint);

        return resultBitmap;
    }

    public static Bitmap contrastBitmap(Bitmap bitmap, float contrast) {
        float[] colorTransform = new float[]{
                contrast, 0, 0, 0, 0,
                0, contrast, 0, 0, 0,
                0, 0, contrast, 0, 0,
                0, 0, 0, 1, 0};

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0f);
        colorMatrix.set(colorTransform);

        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);

        Bitmap resultBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, paint);

        return resultBitmap;
    }

    public static Bitmap saturationBitmap(Bitmap bitmap, float saturation) {

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(saturation);

        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);

        Bitmap resultBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, paint);

        return resultBitmap;
    }


    //todo just testing some stuff don't touch for now

//    @Override
//    public void onBackPressed() {
//        pickedImageBitmap = temporarySaveBitmap;
//    }
}
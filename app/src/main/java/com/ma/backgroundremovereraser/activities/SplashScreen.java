package com.ma.backgroundremovereraser.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;

import com.ma.backgroundremovereraser.R;

public class SplashScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rlGetStarted;
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        rlGetStarted = findViewById(R.id.get_start_rl);
        rlGetStarted.setOnClickListener(this);

        new Handler().postDelayed(() -> rlGetStarted.setVisibility(View.VISIBLE), SPLASH_TIME_OUT);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.get_start_rl:
                startActivity(new Intent(SplashScreen.this,MainActivity.class));
                finish();
                break;

        }

    }
}